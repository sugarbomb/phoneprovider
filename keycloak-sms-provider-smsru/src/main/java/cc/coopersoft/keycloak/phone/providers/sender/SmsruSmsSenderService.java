package cc.coopersoft.keycloak.phone.providers.sender;

import cc.coopersoft.keycloak.phone.providers.exception.MessageSendException;
import cc.coopersoft.keycloak.phone.providers.spi.FullSmsSenderAbstractService;
import org.jboss.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.keycloak.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.util.retry.Retry;

import java.time.Duration;


public class SmsruSmsSenderService extends FullSmsSenderAbstractService {

    private final Config.Scope scope;

    private static final Logger logger = Logger.getLogger(SmsruSmsSenderService.class);

    public SmsruSmsSenderService(String realmDisplay, Config.Scope scope) {
        super(realmDisplay);
        this.scope = scope;
    }

    @Bean
    public WebClient smsruWebClient() {
        return WebClient.builder()
                .baseUrl(scope.get("provider"))
                .build();
    }

    @Override
    public void sendMessage(String phoneNumber, String message) throws MessageSendException {

        try {
            String response = smsruWebClient()
                    .post()
                    .uri(uriBuilder -> uriBuilder
                            .queryParam("api_id", scope.get("apiid"))
                            .queryParam("to", phoneNumber)
                            .queryParam("msg", message)
                            .queryParam("json", "1")
                            .build())
                    .retrieve()
                    .bodyToMono(String.class)
                    .retryWhen(Retry.backoff(3, Duration.ofSeconds(2)))
                    .block();

            logResponseStatus(response, phoneNumber);


        } catch (Exception e) {
            throw new MessageSendException(500, "500", e.getMessage());
        }

    }

    public void logResponseStatus(String response, String phoneNumber) {
        try {
            JSONObject jo = new JSONObject(response);
            if (jo.getString("status").equalsIgnoreCase("ok") &&
                    jo.getJSONObject("sms").getJSONObject(phoneNumber).getString("status").equalsIgnoreCase("ok")) {//jo.getString("status").equalsIgnoreCase("ok")) {
                logger.debug(response);
            } else {
                logger.error(response);
            }
        } catch (JSONException e) { logger.error(String.format("Response: %s had thrown JSONException: %s", response, e.getMessage()));}
    }


    @Override
    public void close() {
    }
}
