# Database integration



+ Cli params
```shell
  kc.[sh|bat] start \

    --spi-authenticator-everybody-phone-authenticator-dburl={DB_URL}
    --spi-authenticator-everybody-phone-authenticator-dbuser={DB_USER}
    --spi-authenticator-everybody-phone-authenticator-dbpassword={DB_PASSWORD}

```

