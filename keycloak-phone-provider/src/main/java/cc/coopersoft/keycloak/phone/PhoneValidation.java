package cc.coopersoft.keycloak.phone;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import javax.ws.rs.BadRequestException;


public class PhoneValidation {

    private final static PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    public static String validatePhoneNumber(String phoneNumber) {
        try {
            PhoneNumber number = phoneNumberUtil.parse(
                    "+" + phoneNumber, PhoneNumber.CountryCodeSource.UNSPECIFIED.name());
            if (!phoneNumberUtil.isValidNumber(number)) throw new BadRequestException("Not a valid number");
            return String.format("%s%s", number.getCountryCode(), number.getNationalNumber());

        } catch (NumberParseException e) {
            throw new BadRequestException(e.getMessage());
        }
    }
}
