package cc.coopersoft.keycloak.phone.authentication.authenticators.directgrant;

import cc.coopersoft.keycloak.phone.smcore.data.dao.UserDAO;
import cc.coopersoft.keycloak.phone.smcore.data.database.ConnectionFactory;
import cc.coopersoft.keycloak.phone.smcore.data.database.StatementExecutor;
import cc.coopersoft.keycloak.phone.smcore.data.mapper.UserMapper;
import cc.coopersoft.keycloak.phone.smcore.impl.DefaultSmCoreProvider;
import org.keycloak.Config;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.authentication.ConfigurableAuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;

import java.util.List;

public class EverybodyPhoneAuthenticatorFactory implements AuthenticatorFactory, ConfigurableAuthenticatorFactory {


  public static final String PROVIDER_ID = "everybody-phone-authenticator";

  private Config.Scope scope;

  @Override
  public Authenticator create(KeycloakSession session){
    var connectionFactory = new ConnectionFactory(scope);
    var statementExecutor = new StatementExecutor(connectionFactory);
    var userDAO = new UserDAO(statementExecutor, new UserMapper());
    var defaultSmCoreProvider = new DefaultSmCoreProvider(userDAO);
    return new EverybodyPhoneAuthenticator(session, defaultSmCoreProvider);  }

  @Override
  public void init(Config.Scope config) {
    this.scope = config;

  }

  @Override
  public void postInit(KeycloakSessionFactory factory) {

  }

  @Override
  public void close() {

  }


  @Override
  public String getDisplayType() {
    return "Authentication everybody by phone";
  }

  @Override
  public String getHelpText() {
    return "Authentication everybody by phone";
  }

  @Override
  public List<ProviderConfigProperty> getConfigProperties() {
    return null;
  }

  @Override
  public String getReferenceCategory() {
    return "Everybody phone Grant";
  }

  @Override
  public boolean isConfigurable() {
    return false;
  }

  private static AuthenticationExecutionModel.Requirement[] REQUIREMENT_CHOICES = {
      AuthenticationExecutionModel.Requirement.REQUIRED,
      AuthenticationExecutionModel.Requirement.DISABLED
  };

  @Override
  public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
    return REQUIREMENT_CHOICES;
  }

  @Override
  public boolean isUserSetupAllowed() {
    return true;
  }

  @Override
  public String getId() {
    return PROVIDER_ID;
  }
}
