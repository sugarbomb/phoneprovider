package cc.coopersoft.keycloak.phone.smcore.configurations;


public final class Configuration {

public final static String USER_GET_QUERY = "SELECT id, keycloak_id FROM clients WHERE phone=? AND tenant_key=?";

public final static String USER_UPDATE_QUERY = "UPDATE clients SET keycloak_id=? WHERE phone=? AND tenant_key=?";

public final static String USER_CREATE_QUERY =
        "INSERT INTO clients (id, keycloak_id, phone, tenant_key, registration_date, last_login, category_id) " +
                "values (?, ?, ?, ?, now(), now(), " +
                "(SELECT value::bigint FROM system_properties WHERE key = 'CLIENT_DEFAULT_GROUP_ID' and tenant_key = ?))";

public final static String USER_SMCORE_ID_FIELD = "id";

public final static String USER_KEYCLOAK_ID_FIELD = "keycloak_id";

}
