package cc.coopersoft.keycloak.phone.smcore.data.database;

import lombok.RequiredArgsConstructor;
import org.keycloak.Config;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@RequiredArgsConstructor
public class ConnectionFactory {

    private final Config.Scope scope;

    public Connection getConnection() {

        try {
            Class.forName("org.postgresql.Driver");

            return DriverManager.getConnection(scope.get("dburl"),
                    scope.get("dbuser"), scope.get("dbpassword"));
        } catch (SQLException e) {
            throw new RuntimeException("Error acquiring database connection", e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Error acquiring driver", e);
        }
    }
}
