package cc.coopersoft.keycloak.phone.providers.spi.impl;

import cc.coopersoft.common.OptionalStringUtils;
import cc.coopersoft.keycloak.phone.Utils;
import cc.coopersoft.keycloak.phone.providers.spi.PhoneProvider;
import cc.coopersoft.keycloak.phone.providers.spi.PhoneVerificationCodeProvider;
import cc.coopersoft.keycloak.phone.providers.constants.TokenCodeType;
import cc.coopersoft.keycloak.phone.providers.exception.MessageSendException;
import cc.coopersoft.keycloak.phone.providers.representations.TokenCodeRepresentation;
import cc.coopersoft.keycloak.phone.providers.spi.MessageSenderService;
import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.keycloak.Config.Scope;
import org.keycloak.models.KeycloakSession;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.ServiceUnavailableException;
import java.util.Optional;


public class DefaultPhoneProvider implements PhoneProvider {

    private static final Logger logger = Logger.getLogger(DefaultPhoneProvider.class);
    private final KeycloakSession session;
    private final String service;
    private final int tokenExpiresIn;
    private final int hourMaximum;

    private final Scope config;

    DefaultPhoneProvider(KeycloakSession session, Scope config) {
        this.session = session;
        this.config = config;


        this.service = session.listProviderIds(MessageSenderService.class)
                .stream().filter(s -> s.equals(config.get("service")))
                .findFirst().orElse(
                        session.listProviderIds(MessageSenderService.class)
                                .stream().findFirst().orElse(null)
                );

        if (StringUtils.isBlank(this.service)) {
            logger.error("Message sender service provider not found!");
        }


        this.tokenExpiresIn = config.getInt("tokenExpiresIn", 300);
        this.hourMaximum = config.getInt("hourMaximum", 5);
    }


    @Override
    public void close() {
    }


    private PhoneVerificationCodeProvider getTokenCodeService() {
        return session.getProvider(PhoneVerificationCodeProvider.class);
    }

    @Override
    public boolean isDuplicatePhoneAllowed(String realm) {
        Boolean result = config.getBoolean(realm + "-duplicate-phone", null);
        if (result == null) {
            result = config.getBoolean("duplicate-phone", false);
        }
        return result;
    }

    @Override
    public Optional<String> phoneNumberRegx(String realm) {
        return OptionalStringUtils.ofBlank(OptionalStringUtils.ofBlank(config.get(realm + "-number-regx"))
                .orElse(config.get("number-regx")));
    }

    @Override
    public int sendTokenCode(String phoneNumber, String tenantKey, TokenCodeType type, String channel, String kind) {
        logger.info("send code to:" + phoneNumber + " through channel: " + channel);

        if (getTokenCodeService().isAbusing(phoneNumber, tenantKey, type, hourMaximum)) {
            throw new ForbiddenException("You requested the maximum number of messages the last hour");
        }

//        TokenCodeRepresentation ongoing =
                getTokenCodeService().ongoingProcess(phoneNumber, tenantKey, type, true);
//        if (ongoing != null) {
//            logger.info(String.format("No need of sending a new %s code for %s", type.getLabel(), phoneNumber));
//            return (int) (ongoing.getExpiresAt().getTime() - Instant.now().toEpochMilli()) / 1000;
//        }

        TokenCodeRepresentation token = TokenCodeRepresentation.forPhoneNumberAndTenant(phoneNumber, tenantKey, channel);

        try {
            session.getProvider(MessageSenderService.class, getChannel(token.getChannel())).sendSmsMessage(type, phoneNumber, token.getCode(),
                    tokenExpiresIn, kind);
            getTokenCodeService().persistCode(token, type, tokenExpiresIn);

            logger.info(String.format("Sent %s code to %s over %s through channel: %s", type.getLabel(), phoneNumber, service, token.getChannel()));

        } catch (MessageSendException e) {

            logger.error(String.format("Message sending to %s (tenant: %s) failed with %s: %s",
                    phoneNumber, tenantKey, e.getErrorCode(), e.getErrorMessage()));
            throw new ServiceUnavailableException("Internal server error");
        }

        return tokenExpiresIn;
    }

    private String getChannel(String channel) {


        return session.listProviderIds(MessageSenderService.class)
                .stream().filter(s -> s.equals(channel))
                .findFirst().orElseThrow(() ->
                        new BadRequestException(String.format("No such channel: %s. ", channel))
                );


    }

}
