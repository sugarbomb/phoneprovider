package cc.coopersoft.keycloak.phone.smcore.data.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class User {
    private final String smCoreId;
    private final String keycloakId;
}
