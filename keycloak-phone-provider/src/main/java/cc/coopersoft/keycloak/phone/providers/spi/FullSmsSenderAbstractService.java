package cc.coopersoft.keycloak.phone.providers.spi;

import cc.coopersoft.keycloak.phone.providers.constants.TokenCodeType;
import cc.coopersoft.keycloak.phone.providers.exception.MessageSendException;

public abstract class FullSmsSenderAbstractService implements MessageSenderService{

    private final String realmDisplay;

    public FullSmsSenderAbstractService(String realmDisplay) {
        this.realmDisplay = realmDisplay;
    }

    public abstract void sendMessage(String phoneNumber, String message) throws MessageSendException;


    @Override
    public void sendSmsMessage(TokenCodeType type, String phoneNumber, String code , int expires, String kind) throws MessageSendException{
        //TODO template from keycloak message bundle
        final String MESSAGE = String.format("Код для входа: %s", code);
        sendMessage(phoneNumber, MESSAGE);
    }
}
