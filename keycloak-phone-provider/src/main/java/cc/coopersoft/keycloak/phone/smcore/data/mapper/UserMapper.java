package cc.coopersoft.keycloak.phone.smcore.data.mapper;

import cc.coopersoft.keycloak.phone.smcore.data.model.User;
import lombok.RequiredArgsConstructor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import static cc.coopersoft.keycloak.phone.smcore.configurations.Configuration.*;

@RequiredArgsConstructor
public class UserMapper {



    public Optional<User> map(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            return Optional.of(User.builder()
                    .smCoreId(resultSet.getString(USER_SMCORE_ID_FIELD))
                    .keycloakId(resultSet.getString(USER_KEYCLOAK_ID_FIELD))
                    .build());
        }

        return Optional.empty();
    }
}
