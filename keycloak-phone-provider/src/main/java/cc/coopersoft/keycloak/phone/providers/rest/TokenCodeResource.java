package cc.coopersoft.keycloak.phone.providers.rest;

import cc.coopersoft.keycloak.phone.PhoneValidation;
import cc.coopersoft.keycloak.phone.Utils;
import cc.coopersoft.keycloak.phone.providers.constants.TokenCodeType;
import cc.coopersoft.keycloak.phone.providers.spi.PhoneProvider;
import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.jboss.resteasy.annotations.cache.NoCache;
import org.keycloak.models.KeycloakSession;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

public class TokenCodeResource {

    private static final Logger logger = Logger.getLogger(TokenCodeResource.class);
    protected final KeycloakSession session;
    protected final TokenCodeType tokenCodeType;

    TokenCodeResource(KeycloakSession session, TokenCodeType tokenCodeType) {
        this.session = session;
        this.tokenCodeType = tokenCodeType;
    }


    @GET
    @NoCache
    @Path("")
    @Produces(APPLICATION_JSON)
    public Response getTokenCode(@NotBlank @QueryParam("phoneNumber") String reqPhoneNumber,
                                 @DefaultValue("whatsapp") @QueryParam("channel") String channel,
                                 @DefaultValue("LcCRkN") @QueryParam("tenantKey") String tenantKey,
    //TODO Change DefaultValue on Config.Scope param
                                 @QueryParam("kind") String kind,
                                 @HeaderParam("Origin") String origin) {

        if (StringUtils.isBlank(reqPhoneNumber)) throw new BadRequestException("Must inform a phone number");

        String phoneNumber = PhoneValidation.validatePhoneNumber(reqPhoneNumber);

        if (!Utils.getPhoneNumberRegx(session).map(phoneNumber::matches).orElse(true)) {
            throw new BadRequestException("Phone number is invalid");
        }

        // everybody phones authenticator send AUTH code
        if (!TokenCodeType.REGISTRATION.equals(tokenCodeType) &&
                !TokenCodeType.AUTH.equals(tokenCodeType) &&
                Utils.findUserByPhoneAndTenant(session.users(), session.getContext().getRealm(), phoneNumber, tenantKey).isEmpty()) {
            throw new ForbiddenException("Phone number not found");
        }

        logger.info(String.format("Requested %s code to %s for tenant %s", tokenCodeType.getLabel(), phoneNumber, tenantKey));
        int tokenExpiresIn = session.getProvider(PhoneProvider.class)
                .sendTokenCode(phoneNumber, tenantKey, tokenCodeType, channel, kind);

        String response = String.format("{\"expires_in\":%s}", tokenExpiresIn);

        return Response.ok(response, APPLICATION_JSON_TYPE)
                //fixme
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Expose-Headers", "Access-Control-Allow-Methods")
                .header("Access-Control-Allow-Origin", origin)
                .build();
    }
}
