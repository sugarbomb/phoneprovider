package cc.coopersoft.keycloak.phone.smcore.data.dao;

import cc.coopersoft.keycloak.phone.smcore.data.database.StatementExecutor;
import cc.coopersoft.keycloak.phone.smcore.data.mapper.UserMapper;
import cc.coopersoft.keycloak.phone.smcore.data.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

import static cc.coopersoft.keycloak.phone.smcore.configurations.Configuration.*;

@Slf4j
@RequiredArgsConstructor
public class UserDAO {

    private final StatementExecutor executor;
    private final UserMapper mapper;

    public Optional<User> findByPhoneAndTenant(String phone, String tenantKey) {
        return executor.executeStatement(connection -> {
            try {
                var statement = connection.prepareStatement(USER_GET_QUERY);
                statement.setString(1, phone);
                statement.setString(2, tenantKey);
                statement.execute();

                var resultSet = statement.getResultSet();

                return mapper.map(resultSet);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void updateKeycloakId(String keycloakId, String phone, String tenantKey) {
         executor.executeStatement(connection -> {
            try {
                var statement = connection.prepareStatement(USER_UPDATE_QUERY);
                statement.setObject(1, UUID.fromString(keycloakId),java.sql.Types.OTHER);
                statement.setString(2, phone);
                statement.setString(3, tenantKey);
                return statement.executeUpdate();
            }catch (SQLException e) {
                throw new RuntimeException(e);
            }

        });
    }

    public void createSmCoreUser(String keycloakId, String phone, String tenantKey) {
        executor.executeStatement(connection -> {
            try {
                var statement = connection.prepareStatement(USER_CREATE_QUERY);
                statement.setObject(1, UUID.fromString(keycloakId),java.sql.Types.OTHER);
                statement.setObject(2, UUID.fromString(keycloakId),java.sql.Types.OTHER);
                statement.setString(3, phone);
                statement.setString(4, tenantKey);
                statement.setString(5, tenantKey);
                return statement.executeUpdate();
            }catch (SQLException e) {
                throw new RuntimeException(e);
            }

        });
    }


}
