package cc.coopersoft.keycloak.phone.authentication.authenticators.directgrant;

import cc.coopersoft.keycloak.phone.authentication.authenticators.browser.SmsOtpMfaAuthenticator;
import cc.coopersoft.keycloak.phone.credential.PhoneOtpCredentialProvider;
import cc.coopersoft.keycloak.phone.providers.constants.TokenCodeType;
import cc.coopersoft.keycloak.phone.providers.representations.TokenCodeRepresentation;
import cc.coopersoft.keycloak.phone.providers.spi.PhoneVerificationCodeProvider;
import cc.coopersoft.keycloak.phone.Utils;
import cc.coopersoft.keycloak.phone.smcore.impl.DefaultSmCoreProvider;
import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.protocol.oidc.OIDCLoginProtocol;

import javax.ws.rs.core.Response;
import java.util.Optional;


public class EverybodyPhoneAuthenticator extends BaseDirectGrantAuthenticator {

  private final DefaultSmCoreProvider defaultSmCoreProvider;


    public EverybodyPhoneAuthenticator(KeycloakSession session, DefaultSmCoreProvider defaultSmCoreProvider) {
    if (session.getContext().getRealm() == null) {
      throw new IllegalStateException("The service cannot accept a session without a realm in its context.");
    }
    this.defaultSmCoreProvider = defaultSmCoreProvider;
  }

  @Override
  public boolean requiresUser() {
    return false;
  }

  @Override
  public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {

  }

    private static final Logger logger = Logger.getLogger(EverybodyPhoneAuthenticator.class);


    @Override
  public void authenticate(AuthenticationFlowContext context) {
    getPhoneNumber(context)
            .ifPresentOrElse(phoneNumber -> getAuthenticationCode(context)
                            .ifPresentOrElse(code -> authToUser(context, phoneNumber, getNullableTenantKey(context), code),
                                    () -> invalidCredentials(context)),
                    () -> invalidCredentials(context));
  }

  private void authToUser(AuthenticationFlowContext context, String phoneNumber, String tenantKey, String code) {
    PhoneVerificationCodeProvider phoneVerificationCodeProvider = context.getSession().getProvider(PhoneVerificationCodeProvider.class);

      //fixme delete after updates
      //fixme change default to scope
      String tenantKeyNN = tenantKey == null || tenantKey.isBlank() ? "LcCRkN" : tenantKey;
      String username = Utils.username(phoneNumber, tenantKeyNN);
      if (tenantKey == null || tenantKey.isBlank()) {
          logger.info("Tenant key is null, auth with default value");
          Optional.ofNullable(context.getSession().users().getUserByUsername(context.getRealm(), phoneNumber))
                  .ifPresent(u -> {
                      u.setUsername(username);
                      u.setSingleAttribute("tenantKey", tenantKeyNN);
                      logger.info("Updated username: " + u.getUsername());
                  });
      }

    TokenCodeRepresentation tokenCode = phoneVerificationCodeProvider.ongoingProcess(phoneNumber, tenantKeyNN, TokenCodeType.AUTH);

    if (tokenCode == null || !tokenCode.getCode().equals(code)) {
      invalidCredentials(context);
      return;
    }

    UserModel user = Utils.findUserByPhoneAndTenant(context.getSession().users(), context.getRealm(), phoneNumber, tenantKeyNN)
        .orElseGet(() -> {
          if (context.getSession().users().getUserByUsername(context.getRealm(),username) != null) {
            invalidCredentials(context, AuthenticationFlowError.USER_CONFLICT);
            return null;
          }
          UserModel newUser = context.getSession().users().addUser(context.getRealm(), username);

          newUser.setEnabled(true);
          context.getAuthenticationSession().setClientNote(OIDCLoginProtocol.LOGIN_HINT_PARAM, username);

          //FIXME
            try {
                defaultSmCoreProvider.userToSmCoreDb(newUser.getId(), phoneNumber, tenantKeyNN);
            } catch (Exception e) {
                    logger.info("Cannot write new user to sm database. Cause: " + e.getCause() + ", Message: " + e.getMessage());
                    Response challenge = errorResponse(Response.Status.CONFLICT.getStatusCode(), "invalid_grant", "Invalid user credentials");
                    context.failure(AuthenticationFlowError.USER_CONFLICT, challenge);
                    return null;
            }
          return newUser;
        });
    if (user != null) {
      context.setUser(user);
      phoneVerificationCodeProvider.tokenValidated(user, phoneNumber, tenantKeyNN, tokenCode.getId());
      context.success();

    }
  }
}
