package cc.coopersoft.keycloak.phone.providers.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.keycloak.models.utils.KeycloakModelUtils;

import java.security.SecureRandom;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenCodeRepresentation {

    private String id;
    private String phoneNumber;
    private String tenantKey;
    private String code;
    private String type;
    private Date createdAt;
    private Date expiresAt;
    private Boolean confirmed;
    private String channel;

    public static TokenCodeRepresentation forPhoneNumberAndTenant(String phoneNumber, String tenantKey, String channel) {

        TokenCodeRepresentation tokenCode = new TokenCodeRepresentation();

        tokenCode.id = KeycloakModelUtils.generateId();
        tokenCode.phoneNumber = phoneNumber;
        tokenCode.tenantKey = tenantKey;
        tokenCode.code = generateTokenCode(phoneNumber);
        tokenCode.confirmed = false;
        tokenCode.channel = channel;

        return tokenCode;
    }

    private static String generateTokenCode(String phoneNumber) {
        if (phoneNumber.equals("79951111111")) { //юзер для модерации эпл с одимнаковым паролем
            return String.format("%04d", 5629);
        } else {
            SecureRandom secureRandom = new SecureRandom();
            Integer code = secureRandom.nextInt(99_99);
            return String.format("%04d", code);
        }
    }
}
