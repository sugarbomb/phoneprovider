package cc.coopersoft.keycloak.phone.smcore.impl;

import cc.coopersoft.keycloak.phone.smcore.data.dao.UserDAO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class DefaultSmCoreProvider {


    private final UserDAO userDAO;


    public void userToSmCoreDb(String keycloakId, String phone, String tenantKey) {

        userDAO.findByPhoneAndTenant(phone, tenantKey)
                .ifPresentOrElse((user)
                                -> {
                    log.info("user found: "+ user.getSmCoreId());
                            if (user.getKeycloakId() == null || !user.getKeycloakId().equals(keycloakId)) userDAO.updateKeycloakId(keycloakId, phone, tenantKey);
                        },
                        () -> {
                            log.info(String.format("user not found with phone %s, tenant: %s. Creating...", phone, tenantKey));
                            userDAO.createSmCoreUser(keycloakId, phone, tenantKey);
                        });


    }

}
