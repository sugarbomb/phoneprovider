package cc.coopersoft.keycloak.phone.providers.sender;

import cc.coopersoft.keycloak.phone.providers.exception.MessageSendException;
import cc.coopersoft.keycloak.phone.providers.spi.FullSmsSenderAbstractService;

import org.jboss.logging.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.util.retry.Retry;
import java.time.Duration;
import java.util.Map;



public class TelegramSmsSenderService extends FullSmsSenderAbstractService {

    private final Logger logger = Logger.getLogger(TelegramSmsSenderService.class);





    public TelegramSmsSenderService(String realmDisplay) {
        super(realmDisplay);

    }

    @Bean
    public WebClient telegramWebClient() {
        return WebClient.builder()
                .baseUrl("https://api.telegram.org/bot5686382087:AAHZ1SZXdWJEpsYRZC4pHMy6-XX6notq8SQ/sendMessage")
                .build();
    }

    @Override
    public void sendMessage(String phone, String message) throws MessageSendException {

        try {

            String response = telegramWebClient()
                    .post()
                    .header("Content-Type", "application/json")
                    .body(BodyInserters.fromValue(Map.of("chat_id",  -811389114, "text", String.format(
                            "%s\n%s", phone, message))))
                    .retrieve()
                    .bodyToMono(String.class)
                    .retryWhen(Retry.backoff(3, Duration.ofSeconds(2)))
                    .block();

            logger.info("Response: " + response);


        } catch (Exception e) {
            throw new MessageSendException(500, "500", e.getMessage());
        }

    }


    @Override
    public void close() {
    }
}
