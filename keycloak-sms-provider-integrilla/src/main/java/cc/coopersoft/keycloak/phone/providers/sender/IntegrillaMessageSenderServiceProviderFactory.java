package cc.coopersoft.keycloak.phone.providers.sender;

import cc.coopersoft.keycloak.phone.providers.spi.MessageSenderService;
import cc.coopersoft.keycloak.phone.providers.spi.MessageSenderServiceProviderFactory;
import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;



public class IntegrillaMessageSenderServiceProviderFactory implements MessageSenderServiceProviderFactory {

    private Config.Scope scope;

    @Override
    public MessageSenderService create(KeycloakSession keycloakSession) {
        return new IntegrillaSmsSenderService(keycloakSession.getContext().getRealm().getDisplayName(), scope);
    }

    @Override
    public void init(Config.Scope scope) {
        this.scope = scope;
    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {
    }

    @Override
    public void close() {
    }

    @Override
    public String getId() {
        return "whatsapp";
    }
}
