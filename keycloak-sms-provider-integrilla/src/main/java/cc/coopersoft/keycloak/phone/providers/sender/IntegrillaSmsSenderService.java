package cc.coopersoft.keycloak.phone.providers.sender;

import cc.coopersoft.keycloak.phone.providers.exception.MessageSendException;
import cc.coopersoft.keycloak.phone.providers.spi.FullSmsSenderAbstractService;
import org.jboss.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.keycloak.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.Map;


public class IntegrillaSmsSenderService extends FullSmsSenderAbstractService {

    private final Config.Scope scope;

    private static final Logger logger = Logger.getLogger(IntegrillaSmsSenderService.class);

    public IntegrillaSmsSenderService(String realmDisplay, Config.Scope scope) {
        super(realmDisplay);
        this.scope = scope;
    }

    @Bean
    public WebClient integrillaWebClient() {
        return WebClient.builder()
                .baseUrl(scope.get("provider"))
                .build();
    }

    @Override
    public void sendMessage(String phoneNumber, String message) throws MessageSendException {

        try {

            String response = integrillaWebClient()
                    .post()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromValue(requestBody(phoneNumber, message)))
                    .retrieve()
                    .bodyToMono(String.class)
                    .retryWhen(Retry.backoff(3, Duration.ofSeconds(2)))
                    .block();

            logResponseStatus(response, phoneNumber);

        } catch (Exception e) {
            throw new MessageSendException(500, "500", e.getMessage());
        }

    }

    private Map<String, String> requestBody(String phoneNumber, String message) {

        return Map.of(
                "source", "whatsapp",
                "token", scope.get("token"),
                "login", scope.get("login"),
                "msg_text", message,
                "msg_to", phoneNumber
        );
    }

    public void logResponseStatus(String response, String phoneNumber) {
        try {
            JSONObject jo = new JSONObject(response);
            if (jo.getString("status").equalsIgnoreCase("ok")) {
                logger.debug(response);
            } else {
                logger.error(response);
            }
        } catch (JSONException e) {
            logger.error(String.format("Response: %s had thrown JSONException: %s", response, e.getMessage()));
        }
    }


    @Override
    public void close() {
    }
}
