# Integrilla Phone Provider



+ Cli params
```shell
  kc.[sh|bat] start \
   # --spi-phone-default-service=whatsapp  #Not needed (provider is chosen by @QueryParam "channel")
    --spi-message-sender-service-whatsapp-provider=https://controller.touch-api.com/api/sendMessage # baseUrl
    --spi-message-sender-service-whatsapp-source=whatsapp #Default
    --spi-message-sender-service-whatsapp-login=YOUR_INTEGRILLA_LOGIN
    --spi-message-sender-service-whatsapp-token=YOUR_INTEGRILLA_TOKEN 
  
```
